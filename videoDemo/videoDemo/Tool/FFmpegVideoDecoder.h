//
//  FFmpegVideoDecoder.h
//  videoDemo
//
//  Created by lihao on 2021/4/10.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
// FFmpeg Header File
#ifdef __cplusplus
extern "C" {
#endif
    
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"
#include "libavutil/avutil.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"
#include "libavutil/opt.h"
    
#ifdef __cplusplus
};
#endif

@protocol FFmpegVideoDecoderDelegate <NSObject>
@optional
- (void)getDecodeVideoDataByFFmpeg:(CMSampleBufferRef)sampleBuffer;

@end

@interface FFmpegVideoDecoder : NSObject

@property (weak, nonatomic) id<FFmpegVideoDecoderDelegate> delegate;

- (instancetype)initWithFormatContext:(AVFormatContext *)formatContext videoStreamIndex:(int)videoStreamIndex;
- (void)startDecodeVideoDataWithAVPacket:(AVPacket)packet;
- (void)stopDecoder;

@end

