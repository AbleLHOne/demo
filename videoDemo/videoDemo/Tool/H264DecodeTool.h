//
//  H264DecodeTool.h
//  videoDemo
//
//  Created by lihao on 2021/4/10.
//


#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <VideoToolbox/VideoToolbox.h>

@protocol  H264DecodeFrameCallbackDelegate <NSObject>

//回调sps和pps数据
- (void)gotDecodedFrame:(CVImageBufferRef )imageBuffer;

@end

@interface H264DecodeTool : NSObject

-(BOOL)initH264Decoder;


-(void)decodeFile:(NSString*)fileName fileExt:(NSString*)fileExt;

//解码nalu
-(void)decodeNalu:(uint8_t *)frame size:(uint32_t)frameSize;

- (void)endDecode;

@property (weak, nonatomic) id<H264DecodeFrameCallbackDelegate> delegate;

@end

