//
//  VideoFileParser.h
//  videoDemo
//
//  Created by lihao on 2021/4/10.
//

#include <objc/NSObject.h>

@interface VideoPacket : NSObject

@property uint8_t* buffer;
@property NSInteger size;
@property NSInteger isFoure;

@end

@interface VideoFileParser : NSObject

-(BOOL)open:(NSString*)fileName;
-(VideoPacket *)nextPacket;
-(void)close;

@end

