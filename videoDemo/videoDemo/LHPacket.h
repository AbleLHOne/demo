//
//  LHPacket.h
//  videoDemo
//
//  Created by lihao on 2021/4/11.
//

#import <Foundation/Foundation.h>
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"
NS_ASSUME_NONNULL_BEGIN

@interface LHPacket : NSObject

@property(nonatomic,assign)AVPacket    packet;

@end

NS_ASSUME_NONNULL_END
