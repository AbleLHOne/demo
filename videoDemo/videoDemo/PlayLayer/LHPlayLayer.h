//
//  LHPlayLayer.h
//  videoDemo
//
//  Created by lihao on 2021/4/10.
//

#import <QuartzCore/QuartzCore.h>
#include <CoreVideo/CoreVideo.h>
NS_ASSUME_NONNULL_BEGIN

@interface LHPlayLayer : CAEAGLLayer
@property CVPixelBufferRef pixelBuffer;
- (id)initWithFrame:(CGRect)frame;
- (void)resetRenderBuffer;
@end

NS_ASSUME_NONNULL_END
