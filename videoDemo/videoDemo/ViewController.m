//
//  ViewController.m
//  videoDemo
//
//  Created by lihao on 2021/4/10.
//

#import "ViewController.h"
#import "H264DecodeTool.h"
#import "LHPlayLayer.h"
#import "LHAVParseHandler.h"
#import "LHAVParseHandler.h"
#import "FFmpegVideoDecoder.h"
#import <KTVHTTPCache.h>


@interface ViewController ()<H264DecodeFrameCallbackDelegate,FFmpegVideoDecoderDelegate,KTVHCDataLoaderDelegate>

@property(nonatomic,strong)UIButton             *playBtn;
@property(nonatomic,strong)LHPlayLayer          *playLayer;  //解码后播放layer
@property(nonatomic,strong)UIProgressView       *progressView;
@property(nonatomic,strong)KTVHCDataLoader      *downLoad;
@property(nonatomic,assign)BOOL                 isPlay;

@end

@implementation ViewController


#pragma mark  -- Life

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
    [KTVHTTPCache proxyStart:nil];
}


#pragma mark  -- configUI

-(void)configUI{
    
    [self.view addSubview:self.playBtn];
}

- (void)initPlayLayer{
    CGFloat height = (self.view.frame.size.height - 100)/2.0 - 20;
    CGFloat width = self.view.frame.size.width;
    self.playLayer = [[LHPlayLayer alloc] initWithFrame:CGRectMake(0 ,256,width,height)];
    self.playLayer.backgroundColor = [UIColor redColor].CGColor;
    [self.view.layer addSublayer:self.playLayer];
    
    [self.view addSubview:self.progressView];
}

#pragma mark  -- 解码回调
- (void)gotDecodedFrame:(CVImageBufferRef)imageBuffer{
    if(imageBuffer)
    {
        //解码回来的数据绘制播放
        self.playLayer.pixelBuffer = imageBuffer;
        CVPixelBufferRelease(imageBuffer);
    }
}


-(void)getDecodeVideoDataByFFmpeg:(CMSampleBufferRef)sampleBuffer {
    CVPixelBufferRef pix = CMSampleBufferGetImageBuffer(sampleBuffer);
    self.playLayer.pixelBuffer = pix;
   
}

#pragma mark - KTVHCDataLoaderDelegate

- (void)ktv_loaderDidFinish:(KTVHCDataLoader *)loader{
    
    
}
- (void)ktv_loader:(KTVHCDataLoader *)loader didFailWithError:(NSError *)error{
    
    
}

- (void)ktv_loader:(KTVHCDataLoader *)loader didChangeProgress:(double)progress{
    
    NSLog(@"%f",progress);
   
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [ self.progressView setProgress:progress animated:YES];
    });

}


#pragma mark -- E

-(void)playBtnAction{
    
    
    if (self.isPlay) {
        
        return;
    }
    NSURL *originalURL = [NSURL URLWithString:@"https://cdn.suapp.mobi/app_test/outfile.h264"];
    NSURL *proxyURL = [KTVHTTPCache proxyURLWithOriginalURL:originalURL];
    [self downLoadVideoWithVideoURL:proxyURL];
    [self initPlayLayer];
    [self startDecodeByFFmpegH264DataWithPtah:[proxyURL absoluteString]];
    
}

- (void)startDecodeByFFmpegH264DataWithPtah:(NSString*)videoPath{

    LHAVParseHandler *parseHandler = [[LHAVParseHandler alloc] initWithPath:videoPath];
    FFmpegVideoDecoder *decoder = [[FFmpegVideoDecoder alloc] initWithFormatContext:[parseHandler getFormatContext] videoStreamIndex:[parseHandler getVideoStreamIndex]];
    decoder.delegate = self;
    __weak __typeof(self)weakSelf = self;
    [parseHandler startParseGetAVPackeWithCompletionHandler:^(BOOL isVideoFrame, BOOL isFinish, AVPacket packet,double currentTime,double duration) {
    
        if (isFinish) {
            [decoder stopDecoder];
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.isPlay =NO;
                [KTVHTTPCache cacheDeleteAllCaches];
                [ weakSelf.progressView setProgress:0 animated:YES];
            });
            
            return;
        }
        
        if (isVideoFrame) {
            weakSelf.isPlay =YES;
            [decoder startDecodeVideoDataWithAVPacket:packet];
        }
    }];
    
}


-(void)downLoadVideoWithVideoURL:(NSURL*)videoURL{
    
    KTVHCDataRequest *req = [[KTVHCDataRequest alloc] initWithURL:videoURL headers:[NSDictionary dictionary]];
   self.downLoad = [KTVHTTPCache cacheLoaderWithRequest:req];
   self.downLoad.delegate = self;
   [self.downLoad prepare];
    
    
}



#pragma mark -- getter--setter


-(UIButton *)playBtn{
    
    if (!_playBtn) {

        _playBtn = [[UIButton alloc]initWithFrame:CGRectMake(160, 130, 100, 50)];
        [_playBtn setBackgroundColor:[UIColor cyanColor]];
        [_playBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_playBtn setTitle:@"播放" forState:UIControlStateNormal];
        [_playBtn addTarget:self action:@selector(playBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _playBtn;
}



-(UIProgressView *)progressView{
    
    if (!_progressView) {
        
        _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.playLayer.frame), self.playLayer.frame.size.width, 10)];
        _progressView.backgroundColor = [UIColor grayColor];
        _progressView.progressViewStyle = UIProgressViewStyleDefault;
        _progressView.progressTintColor = [UIColor orangeColor];
    }
    
    return _progressView;
    
}



@end
